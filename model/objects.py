# coding: utf-8
from sqlalchemy import Column, Date, DateTime, ForeignKey, Integer, String, TIMESTAMP, text
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy_serializer import SerializerMixin

Base = declarative_base(cls=(SerializerMixin))
metadata = Base.metadata

class ApiToken(Base):
    __tablename__ = 'api_token'

    api_token_id = Column(Integer, primary_key=True)
    token = Column(String(255), nullable=False)
    created_at = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))
    expires_at = Column(TIMESTAMP)
    name = Column(String(60))

class SystemUser(Base):
    __tablename__ = 'system_user'

    user_id = Column(Integer, primary_key=True)
    email = Column(String(60), nullable=False, unique=True)
    password = Column(String(255), nullable=False)
    first_name = Column(String(60))
    last_name = Column(String(60))


class AuthToken(Base):
    __tablename__ = 'auth_token'

    auth_token_id = Column(Integer, primary_key=True)
    user_id = Column(ForeignKey('system_user.user_id'), nullable=False, index=True)
    token = Column(String(255), nullable=False)
    created_at = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))
    expires_at = Column(TIMESTAMP)
    device_id = Column(String(255))

    user = relationship('SystemUser')


class Category(Base):
    __tablename__ = 'category'

    category_id = Column(Integer, primary_key=True)
    user_id = Column(ForeignKey('system_user.user_id'), nullable=False, index=True)
    name = Column(String(100), nullable=False)
    color = Column(String(10))
    icon = Column(String(20))

    user = relationship('SystemUser')


class PasswordReset(Base):
    __tablename__ = 'password_reset'

    password_reset_id = Column(Integer, primary_key=True)
    user_id = Column(ForeignKey('system_user.user_id'), nullable=False, index=True)
    code = Column(String(10), nullable=False)
    token = Column(String(255), nullable=False)
    created_at = Column(TIMESTAMP, nullable=False, server_default=text("CURRENT_TIMESTAMP"))
    consumed = Column(Integer, nullable=False, server_default=text("'0'"))
    consumed_at = Column(TIMESTAMP)

    user = relationship('SystemUser')


class Entry(Base):
    __tablename__ = 'entry'

    entry_id = Column(Integer, primary_key=True)
    category_id = Column(ForeignKey('category.category_id'), nullable=False, index=True)
    date_entered = Column(Date, nullable=False)
    start_time = Column(DateTime)
    end_time = Column(DateTime)
    manual_minutes = Column(Integer)
    calculated_minutes = Column(Integer)

    category = relationship('Category')
