from sqlalchemy import create_engine, MetaData 
from sqlalchemy.orm import sessionmaker, scoped_session, query 
from model.objects import Base, metadata 

engine = create_engine("mysql+pymysql://tasktime:Wy0Buffal0307@meridia.crooktec.com/task_x_time")
engine.connect()

sessionMaker = sessionmaker(autocommit=False,autoflush=False,bind=engine)
db_session = scoped_session(sessionMaker)
Base.query = db_session.query_property()

