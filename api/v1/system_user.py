from flask import Flask, request, Blueprint, jsonify, Response
from model.objects import *
from model.db import db_session
from datetime import datetime
from api.api_utils import tokenIsValid
from werkzeug.security import generate_password_hash, check_password_hash
import json

# Set blueprint
v1SystemUser = Blueprint('v1SystemUser', __name__, url_prefix='/api/v1/SystemUser')

@v1SystemUser.route('', methods=['GET'])
def get():
    # Authenticate with the API token
    headers = request.headers
    if (tokenIsValid(headers.get('api-token'))):
        # Token valid - complete the request

        dbsession = db_session() 
        # Handle parameters
        if (request.args.get('user_id') != None):
            results = dbsession.query(SystemUser).filter(SystemUser.user_id == request.args.get('user_id')).all()
        elif (request.args.get('email') != None):
            results = dbsession.query(SystemUser).filter(SystemUser.email == request.args.get('email')).all()
        else:
            results = dbsession.query(SystemUser).all()
        
        # Finalize results and send
        dbsession.close()
        jsonList = []
        for r in results:
            jsonList.append(r.to_dict())
        return json.dumps(jsonList, indent=4, sort_keys=True, default=str), 200, {'content-type':'application/json'}
    

    else:
            # Token invalid - don't complete the request
            return Response(status=403, mimetype="application/json")

@v1SystemUser.route('', methods=['PUT'])
def put():
    # Authenticate with the API token
    headers = request.headers
    if (tokenIsValid(headers.get('api-token'))):
        # Token valid - complete the request
        data = request.json
        newUser = SystemUser()
        newUser.email = data['email']
        newUser.first_name = data['first_name']
        newUser.last_name = data['last_name']
        newUser.password = generate_password_hash(data['password'],'sha256')
        dbsession = db_session()
        dbsession.add(newUser)
        dbsession.commit()
        dbsession.close()
        return Response(status=201, mimetype="application/json")

    else:
        # Token invalid - don't complete the request
        return Response(status=403, mimetype="application/json")

@v1SystemUser.route('auth', methods=['GET'])
def auth():
    headers = request.headers
    if (tokenIsValid(headers.get('api-token'))):
        # Token valid - complete the request
        data = request.json
        email = data['email']
        password = data['password']
        dbsession = db_session() 
        user = dbsession.query(SystemUser).filter(SystemUser.email == email).one()
        dbsession.close()

        if user != None:
            if check_password_hash(user.password, password):
                return Response(status=200, mimetype="application/json")
            else: 
                return Response(status=403, mimetype="application/json")
        else: 
                return Response(status=404, mimetype="application/json")

    else:
        # Token invalid - don't complete the request
        return Response(status=403, mimetype="application/json")