from flask import Flask, request, Blueprint, jsonify, Response
from model.objects import *
from model.db import db_session
from datetime import datetime
from api.api_utils import tokenIsValid
from werkzeug.security import generate_password_hash, check_password_hash
import json

# Set blueprint
v1Entry = Blueprint('v1Entry', __name__, url_prefix='/api/v1/Entry')

@v1Entry.route('', methods=['GET'])
def get():
    # Authenticate with the API token
    headers = request.headers
    if (tokenIsValid(headers.get('api-token'))):
        # Token valid - complete the request

        dbsession = db_session() 
        # Handle parameters
        if request.args.get('user_id') != None:
            results = dbsession.query(Entry).join(Category).join(SystemUser).filter(SystemUser.user_id == request.args.get('user_id')).all()
        elif request.args.get('category_id') != None:
            results = dbsession.query(Entry).join(Category).join(SystemUser).filter(Entry.category_id == request.args.get('category_id')).all()
        elif request.args.get('user_id') != None and request.args.get('category_id') != None:
            results = dbsession.query(Entry).join(Category).join(SystemUser).filter(SystemUser.user_id == request.args.get('user_id')).filter(Entry.category_id == request.args.get('category_id')).all()
        else:
            results = dbsession.query(Entry).all()
        
        # Finalize results and send
        
        jsonList = []
        for r in results:
            jsonList.append(r.to_dict())
        dbsession.close()
        return json.dumps(jsonList, indent=4, sort_keys=True, default=str), 200, {'content-type':'application/json'}
    

    else:
            # Token invalid - don't complete the request
            return Response(status=403, mimetype="application/json")

@v1Entry.route('', methods=['PUT'])
def put():
    # Authenticate with the API token
    headers = request.headers
    if (tokenIsValid(headers.get('api-token'))):
        # Token valid - complete the request
        data = request.json
        new = Entry()
        new.category_id = data['category_id']
        new.date_entered = data['date_entered']
        new.start_time = data['start_time']
        new.end_time = data['end_time']
        new.manual_minutes = data['manual_minutes']
        dbsession = db_session()
        dbsession.add(new)
        dbsession.commit()
        dbsession.close()
        
        return Response(status=201, mimetype="application/json")
    else:
        # Token invalid - don't complete the request
        return Response(status=403, mimetype="application/json")