from flask import request
from model.objects import *
from model.db import db_session

def tokenIsValid(token):
    # Authenticate with the API token
    headers = request.headers
    token = headers.get('api-token')
    dbsession = db_session()
    validToken = dbsession.query(ApiToken.token).filter(ApiToken.token == token).all()
    dbsession.close() 

    if len(validToken) > 0:
        # Token valid 
        return True
    else:
        return False