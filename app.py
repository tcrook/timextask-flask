from flask import Flask, request 
from flask import render_template 
from model.db import db_session
from model.objects import *


app = Flask(__name__)

# API 
#v1
from api.v1.system_user import v1SystemUser
from api.v1.category import v1Category
from api.v1.entry import v1Entry


# API Blueprints
# v1
app.register_blueprint(v1SystemUser)
app.register_blueprint(v1Category)
app.register_blueprint(v1Entry)

@app.route('/livestatus')
def livestatus():
    dbsession = db_session()
    results = dbsession.query(SystemUser).all()
    dbsession.close()
    if len(results) > 0:
        return '<h1>Server live</h1>'
    else:
        return '<h1>DB not connected</h1>'
    return '<h1>Flask running</h1>'